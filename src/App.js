import React, {useState, useEffect} from 'react';

function App() {
    const [todos, setTodos] = useState({station: {}, stationboard: []});
    const [stadtId, setStadtId] = useState('')

    useEffect(() => {
        fetchData()
        // eslint-disable-next-line
    }, [stadtId])

    async function fetchData() {
        fetch('http://transport.opendata.ch/v1/stationboard?id=' + stadtId + '&limit=10')
            .then(response => response.json())
            .then(todos => {
                setTodos(todos);
            })
            // .then(todos => {
            //     setTodos(
            //         todos.stationboard.map(c => {return {
            //             name: c.name,
            //             to: c.to,
            //             departure: c.stop.departure,
            //             from: c.stop.platform
            //         }})
    }

    return (
        <div>
            <header className="p-3 mb-2 bg-danger text-white">
                <h1 className="display-1">SBB Stationboard</h1>
            </header>
            <main>
                <div className="p-5">
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">Time</th>
                            <th scope="col">ID</th>
                            <th scope="col">To</th>
                            <th scope="col">Platform</th>
                        </tr>
                        </thead>
                        <tbody>
                        {todos.stationboard.map(stationboard => <tr
                            key={stationboard.name}>
                            <td>{new Date(stationboard.stop.departure).toISOString().substring(11,16)}</td>
                            <td>{stationboard.name}</td>
                            <td>{stationboard.to}</td>
                            <td>{stationboard.stop.platform}</td>
                        </tr>)}
                        </tbody>
                    </table>
                    <br/><br/>
                    <p>Choose station:</p>
                    <input type='text' class="form-control" value={stadtId}
                           onChange={(e) => setStadtId(e.target.value)}/>
                </div>
            </main>
        </div>
    );
}


export default App;
